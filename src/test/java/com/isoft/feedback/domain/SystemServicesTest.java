package com.isoft.feedback.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.isoft.feedback.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

public class SystemServicesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SystemServices.class);
        SystemServices systemServices1 = new SystemServices();
        systemServices1.setId(1L);
        SystemServices systemServices2 = new SystemServices();
        systemServices2.setId(systemServices1.getId());
        assertThat(systemServices1).isEqualTo(systemServices2);
        systemServices2.setId(2L);
        assertThat(systemServices1).isNotEqualTo(systemServices2);
        systemServices1.setId(null);
        assertThat(systemServices1).isNotEqualTo(systemServices2);
    }
}

package com.isoft.feedback.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MessagesMapperTest {
    private MessagesMapper messagesMapper;

    @BeforeEach
    public void setUp() {
        messagesMapper = new MessagesMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(messagesMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(messagesMapper.fromId(null)).isNull();
    }
}

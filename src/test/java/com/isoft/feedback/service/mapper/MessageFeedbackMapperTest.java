package com.isoft.feedback.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MessageFeedbackMapperTest {
    private MessageFeedbackMapper messageFeedbackMapper;

    @BeforeEach
    public void setUp() {
        messageFeedbackMapper = new MessageFeedbackMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(messageFeedbackMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(messageFeedbackMapper.fromId(null)).isNull();
    }
}

package com.isoft.feedback.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SystemMapperTest {
    private SystemMapper systemMapper;

    @BeforeEach
    public void setUp() {
        systemMapper = new SystemMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(systemMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(systemMapper.fromId(null)).isNull();
    }
}

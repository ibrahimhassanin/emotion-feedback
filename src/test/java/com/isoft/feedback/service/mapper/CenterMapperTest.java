package com.isoft.feedback.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CenterMapperTest {
    private CenterMapper centerMapper;

    @BeforeEach
    public void setUp() {
        centerMapper = new CenterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(centerMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(centerMapper.fromId(null)).isNull();
    }
}

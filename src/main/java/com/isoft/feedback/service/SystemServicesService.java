package com.isoft.feedback.service;

import com.isoft.feedback.service.dto.SystemServicesDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.isoft.feedback.domain.SystemServices}.
 */
public interface SystemServicesService {
    /**
     * Save a systemServices.
     *
     * @param systemServicesDTO the entity to save.
     * @return the persisted entity.
     */
    SystemServicesDTO save(SystemServicesDTO systemServicesDTO);

    /**
     * Get all the systemServices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SystemServicesDTO> findAll(Pageable pageable);

    /**
     * Get the "id" systemServices.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SystemServicesDTO> findOne(Long id);

    /**
     * Delete the "id" systemServices.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

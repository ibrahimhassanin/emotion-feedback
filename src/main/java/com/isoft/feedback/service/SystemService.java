package com.isoft.feedback.service;

import com.isoft.feedback.service.dto.SystemDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.isoft.feedback.domain.System}.
 */
public interface SystemService {
    /**
     * Save a system.
     *
     * @param systemDTO the entity to save.
     * @return the persisted entity.
     */
    SystemDTO save(SystemDTO systemDTO);

    /**
     * Get all the systems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SystemDTO> findAll(Pageable pageable);

    /**
     * Get the "id" system.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SystemDTO> findOne(Long id);

    /**
     * Delete the "id" system.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

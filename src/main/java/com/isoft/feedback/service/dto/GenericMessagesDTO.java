package com.isoft.feedback.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;

/**
 * A DTO for the {@link com.isoft.feedback.domain.Messages} entity.
 */
@ApiModel(description = "GenericMessagesDTO (hold all the required attributes ).\n@author Khalid Nouh .")
public class GenericMessagesDTO extends MessagesDTO implements Serializable {
    private String systemNameAr;

    private String systemNameEn;

    private String systemCode;

    private String systemServicesNameAr;

    private String systemServicesNameEn;

    private String systemServicesCode;

    private String centerNameAr;

    private String centerNameEn;

    private String centerCode;

    private String userNameAr;

    private String userNameEn;

    private String userCode;

    public String getSystemNameAr() {
        return systemNameAr;
    }

    public void setSystemNameAr(String systemNameAr) {
        this.systemNameAr = systemNameAr;
    }

    public String getSystemNameEn() {
        return systemNameEn;
    }

    public void setSystemNameEn(String systemNameEn) {
        this.systemNameEn = systemNameEn;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getSystemServicesNameAr() {
        return systemServicesNameAr;
    }

    public void setSystemServicesNameAr(String systemServicesNameAr) {
        this.systemServicesNameAr = systemServicesNameAr;
    }

    public String getSystemServicesNameEn() {
        return systemServicesNameEn;
    }

    public void setSystemServicesNameEn(String systemServicesNameEn) {
        this.systemServicesNameEn = systemServicesNameEn;
    }

    public String getSystemServicesCode() {
        return systemServicesCode;
    }

    public void setSystemServicesCode(String systemServicesCode) {
        this.systemServicesCode = systemServicesCode;
    }

    public String getCenterNameAr() {
        return centerNameAr;
    }

    public void setCenterNameAr(String centerNameAr) {
        this.centerNameAr = centerNameAr;
    }

    public String getCenterNameEn() {
        return centerNameEn;
    }

    public void setCenterNameEn(String centerNameEn) {
        this.centerNameEn = centerNameEn;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getUserNameAr() {
        return userNameAr;
    }

    public void setUserNameAr(String userNameAr) {
        this.userNameAr = userNameAr;
    }

    public String getUserNameEn() {
        return userNameEn;
    }

    public void setUserNameEn(String userNameEn) {
        this.userNameEn = userNameEn;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}

package com.isoft.feedback.service;

import com.isoft.feedback.service.dto.MessageFeedbackDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.isoft.feedback.domain.MessageFeedback}.
 */
public interface MessageFeedbackService {
    /**
     * Save a messageFeedback.
     *
     * @param messageFeedbackDTO the entity to save.
     * @return the persisted entity.
     */
    MessageFeedbackDTO save(MessageFeedbackDTO messageFeedbackDTO);

    /**
     * Get all the messageFeedbacks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MessageFeedbackDTO> findAll(Pageable pageable);

    /**
     * Get the "id" messageFeedback.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MessageFeedbackDTO> findOne(Long id);

    /**
     * Delete the "id" messageFeedback.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

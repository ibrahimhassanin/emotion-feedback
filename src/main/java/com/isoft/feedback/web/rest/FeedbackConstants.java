package com.isoft.feedback.web.rest;

public interface FeedbackConstants {
    public static final Integer STATUS_INACTIVE = 2;
    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer TOTAL_RECORDS = 3;
    public static final Integer FIRST_PAGE = 0;
}

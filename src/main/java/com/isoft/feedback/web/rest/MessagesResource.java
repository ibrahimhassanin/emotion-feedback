package com.isoft.feedback.web.rest;

import com.isoft.feedback.service.*;
import com.isoft.feedback.service.dto.*;
import com.isoft.feedback.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link com.isoft.feedback.domain.Messages}.
 */
@RestController
@RequestMapping("/api")
public class MessagesResource {
    private final Logger log = LoggerFactory.getLogger(MessagesResource.class);

    private static final String ENTITY_NAME = "messages";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MessagesService messagesService;

    private final MessagesQueryService messagesQueryService;

    private final SystemService systemService;

    private final SystemServicesService systemServicesService;

    private final CenterService centerService;

    private final UsersService usersService;
    private final MessageFeedbackService messageFeedbackService;

    public MessagesResource(
        MessagesService messagesService,
        MessagesQueryService messagesQueryService,
        SystemService systemService,
        SystemServicesService systemServicesService,
        CenterService centerService,
        UsersService usersService,
        MessageFeedbackService messageFeedbackService
    ) {
        this.messagesService = messagesService;
        this.messagesQueryService = messagesQueryService;
        this.systemService = systemService;
        this.systemServicesService = systemServicesService;
        this.centerService = centerService;
        this.usersService = usersService;
        this.messageFeedbackService = messageFeedbackService;
    }

    /**
     * {@code POST  /messages} : Create a new messages.
     *
     * @param messagesDTO the messagesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new messagesDTO, or with status {@code 400 (Bad Request)} if the messages has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/messages")
    public ResponseEntity<MessagesDTO> createMessages(@RequestBody MessagesDTO messagesDTO) throws URISyntaxException {
        log.debug("REST request to save Messages : {}", messagesDTO);
        if (messagesDTO.getId() != null) {
            throw new BadRequestAlertException("A new messages cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MessagesDTO result = messagesService.save(messagesDTO);
        return ResponseEntity
            .created(new URI("/api/messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/feedback/{messageId}/{feedback}")
    public ResponseEntity<MessageFeedbackDTO> applyFeedbackMessage(
        @PathVariable("messageId") Long messageId,
        @PathVariable("feedback") String feedback
    )
        throws URISyntaxException {
        if (messageId != null && messageId > 0) {
            MessagesDTO tempMessage = messagesService.findOne(messageId).get();
            tempMessage.setStatus(2);
            MessagesDTO updatedMessage = messagesService.save(tempMessage);
            MessageFeedbackDTO messageFeedbackDTO = new MessageFeedbackDTO();
            messageFeedbackDTO.setApplicantName(updatedMessage.getApplicantName());
            messageFeedbackDTO.setCenterId(updatedMessage.getCenterId());
            messageFeedbackDTO.setCounter(updatedMessage.getCounter());
            messageFeedbackDTO.setMessage(updatedMessage.getMessage());
            messageFeedbackDTO.setFeedback(feedback);
            messageFeedbackDTO.setSystemId(updatedMessage.getSystemId());
            messageFeedbackDTO.setSystemServicesId(updatedMessage.getSystemServicesId());
            messageFeedbackDTO.setTrsId(updatedMessage.getTrsId());
            messageFeedbackDTO.setUserId(updatedMessage.getUsersId());
            messageFeedbackDTO.setStatus(1);
            MessageFeedbackDTO savedFeedback = messageFeedbackService.save(messageFeedbackDTO);
            return ResponseEntity
                .created(new URI("/api/messages/" + savedFeedback.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, savedFeedback.getId().toString()))
                .body(savedFeedback);
        }
        return null;
    }

    @PostMapping("/enqueue")
    public ResponseEntity<MessagesDTO> enqueueMessages(@RequestBody GenericMessagesDTO genericMessagesDTO) throws URISyntaxException {
        log.debug("REST request to enqueue Messages : {}", genericMessagesDTO);
        if (genericMessagesDTO.getId() != null) {
            throw new BadRequestAlertException("A new messages cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<SystemDTO> systemRec = systemService.findOne(genericMessagesDTO.getSystemId());
        MessagesDTO tempMessageDto = (MessagesDTO) genericMessagesDTO;
        MessagesDTO result = null;
        UsersDTO savedUser = null;
        //check if the system is found or will create a new system
        if (systemRec.isPresent()) {
            //if the emo system services already exist or will create a new service
            if (systemServicesService.findOne(tempMessageDto.getSystemServicesId()).isPresent()) {
                //check the center is exist
                if (centerService.findOne(tempMessageDto.getCenterId()).isPresent()) {
                    //check the users id is exist
                    if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                        result = messagesService.save(tempMessageDto);
                    } else {
                        savedUser = createUsersDTO(genericMessagesDTO);
                        if (savedUser != null) {
                            tempMessageDto.setUsersId(savedUser.getId());
                            result = messagesService.save(tempMessageDto);
                        }
                    }
                } else {
                    CenterDTO savedCenter = createCenterDto(genericMessagesDTO);
                    if (savedCenter != null) {
                        tempMessageDto.setCenterId(savedCenter.getId());
                        if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                            result = messagesService.save(tempMessageDto);
                        } else {
                            savedUser = createUsersDTO(genericMessagesDTO);
                            if (savedUser != null) {
                                tempMessageDto.setUsersId(savedUser.getId());
                                result = messagesService.save(tempMessageDto);
                            }
                        }
                    }
                }
            } else {
                //create emo system services if the service not found
                SystemServicesDTO tempSystemServicesDTO = createSystemServicesDTO(genericMessagesDTO);
                if (tempSystemServicesDTO != null) {
                    //setting the service id to the generatd id after saving
                    tempMessageDto.setSystemServicesId(tempSystemServicesDTO.getId());
                    if (centerService.findOne(tempMessageDto.getCenterId()).isPresent()) {
                        //check the users id is exist
                        if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                            result = messagesService.save(tempMessageDto);
                        } else {
                            savedUser = createUsersDTO(genericMessagesDTO);
                            if (savedUser != null) {
                                tempMessageDto.setUsersId(savedUser.getId());
                                result = messagesService.save(tempMessageDto);
                            }
                        }
                    } else {
                        CenterDTO savedCenter = createCenterDto(genericMessagesDTO);
                        if (savedCenter != null) {
                            tempMessageDto.setCenterId(savedCenter.getId());
                            if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                                result = messagesService.save(tempMessageDto);
                            } else {
                                savedUser = createUsersDTO(genericMessagesDTO);
                                if (savedUser != null) {
                                    tempMessageDto.setUsersId(savedUser.getId());
                                    result = messagesService.save(tempMessageDto);
                                }
                            }
                        }
                    }
                }
            }
            //if the system not found in the emo_system table will create a new one
        } else {
            SystemDTO resultSystem = createSystemDTO(genericMessagesDTO);
            if (resultSystem != null) {
                genericMessagesDTO.setSystemId(resultSystem.getId());
                //if the emo system services already exist or will create a new service
                if (systemServicesService.findOne(tempMessageDto.getSystemServicesId()).isPresent()) {
                    //check the center is exist
                    if (centerService.findOne(tempMessageDto.getCenterId()).isPresent()) {
                        //check the users id is exist
                        if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                            result = messagesService.save(tempMessageDto);
                        } else {
                            savedUser = createUsersDTO(genericMessagesDTO);
                            if (savedUser != null) {
                                tempMessageDto.setUsersId(savedUser.getId());
                                result = messagesService.save(tempMessageDto);
                            }
                        }
                    } else {
                        CenterDTO savedCenter = createCenterDto(genericMessagesDTO);
                        if (savedCenter != null) {
                            tempMessageDto.setCenterId(savedCenter.getId());
                            if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                                result = messagesService.save(tempMessageDto);
                            } else {
                                savedUser = createUsersDTO(genericMessagesDTO);
                                if (savedUser != null) {
                                    tempMessageDto.setUsersId(savedUser.getId());
                                    result = messagesService.save(tempMessageDto);
                                }
                            }
                        }
                    }
                } else {
                    //create emo system services if the service not found
                    SystemServicesDTO tempSystemServicesDTO = createSystemServicesDTO(genericMessagesDTO);
                    if (tempSystemServicesDTO != null) {
                        //setting the service id to the generatd id after saving
                        tempMessageDto.setSystemServicesId(tempSystemServicesDTO.getId());
                        if (centerService.findOne(tempMessageDto.getCenterId()).isPresent()) {
                            //check the users id is exist
                            if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                                result = messagesService.save(tempMessageDto);
                            } else {
                                savedUser = createUsersDTO(genericMessagesDTO);
                                if (savedUser != null) {
                                    tempMessageDto.setUsersId(savedUser.getId());
                                    result = messagesService.save(tempMessageDto);
                                }
                            }
                        } else {
                            CenterDTO savedCenter = createCenterDto(genericMessagesDTO);
                            if (savedCenter != null) {
                                tempMessageDto.setCenterId(savedCenter.getId());
                                if (usersService.findOne(tempMessageDto.getUsersId()).isPresent()) {
                                    result = messagesService.save(tempMessageDto);
                                } else {
                                    savedUser = createUsersDTO(genericMessagesDTO);
                                    if (savedUser != null) {
                                        tempMessageDto.setUsersId(savedUser.getId());
                                        result = messagesService.save(tempMessageDto);
                                    }
                                }
                            }
                        }
                    }
                }
                //if the system not found in the emo_system table will create a new one
            }
        }

        return ResponseEntity
            .created(new URI("/api/messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /messages} : Updates an existing messages.
     *
     * @param messagesDTO the messagesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated messagesDTO,
     * or with status {@code 400 (Bad Request)} if the messagesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the messagesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/messages")
    public ResponseEntity<MessagesDTO> updateMessages(@RequestBody MessagesDTO messagesDTO) throws URISyntaxException {
        log.debug("REST request to update Messages : {}", messagesDTO);
        if (messagesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MessagesDTO result = messagesService.save(messagesDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, messagesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /messages} : get all the messages.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of messages in body.
     */
    @GetMapping("/messages")
    public ResponseEntity<List<MessagesDTO>> getAllMessages(MessagesCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Messages by criteria: {}", criteria);
        Page<MessagesDTO> page = messagesQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/inquireMessages")
    public ResponseEntity<List<MessagesDTO>> inquireMessages(Pageable pageable) {
        MessagesCriteria criteria = new MessagesCriteria();
        IntegerFilter integerFilter = new IntegerFilter();
        integerFilter.setEquals((FeedbackConstants.STATUS_ACTIVE));
        criteria.setStatus(integerFilter);
        pageable = PageRequest.of(FeedbackConstants.FIRST_PAGE, FeedbackConstants.TOTAL_RECORDS, Sort.by("id").descending());
        log.debug("REST request to inquire Messages  by criteria: {}", criteria);
        Page<MessagesDTO> page = messagesQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /messages/count} : count all the messages.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/messages/count")
    public ResponseEntity<Long> countMessages(MessagesCriteria criteria) {
        log.debug("REST request to count Messages by criteria: {}", criteria);
        return ResponseEntity.ok().body(messagesQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /messages/:id} : get the "id" messages.
     *
     * @param id the id of the messagesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the messagesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/messages/{id}")
    public ResponseEntity<MessagesDTO> getMessages(@PathVariable Long id) {
        log.debug("REST request to get Messages : {}", id);
        Optional<MessagesDTO> messagesDTO = messagesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(messagesDTO);
    }

    /**
     * {@code DELETE  /messages/:id} : delete the "id" messages.
     *
     * @param id the id of the messagesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/messages/{id}")
    public ResponseEntity<Void> deleteMessages(@PathVariable Long id) {
        log.debug("REST request to delete Messages : {}", id);
        messagesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    public CenterDTO createCenterDto(GenericMessagesDTO genericMessagesDTO) {
        CenterDTO tempCenterDto = new CenterDTO();
        tempCenterDto.setCode(genericMessagesDTO.getSystemCode());
        tempCenterDto.setNameAr(genericMessagesDTO.getCenterNameAr());
        tempCenterDto.setNameEn(genericMessagesDTO.getCenterNameEn());
        tempCenterDto.setStatus(1);
        return centerService.save(tempCenterDto);
    }

    public SystemServicesDTO createSystemServicesDTO(GenericMessagesDTO genericMessagesDTO) {
        SystemServicesDTO systemServicesDTO = new SystemServicesDTO();
        systemServicesDTO.setSystemId(genericMessagesDTO.getSystemId());
        systemServicesDTO.setCode(genericMessagesDTO.getSystemServicesCode());
        systemServicesDTO.setNameAr(genericMessagesDTO.getSystemServicesNameAr());
        systemServicesDTO.setNameEn(genericMessagesDTO.getSystemServicesNameEn());
        systemServicesDTO.setStatus(1);
        return systemServicesService.save(systemServicesDTO);
    }

    public SystemDTO createSystemDTO(GenericMessagesDTO genericMessagesDTO) {
        SystemDTO tempSystemDto = new SystemDTO();
        tempSystemDto.setCode(genericMessagesDTO.getSystemCode());
        tempSystemDto.setNameAr(genericMessagesDTO.getSystemNameAr());
        tempSystemDto.setNameEn(genericMessagesDTO.getSystemNameEn());
        tempSystemDto.setStatus(1);
        return systemService.save(tempSystemDto);
    }

    public UsersDTO createUsersDTO(GenericMessagesDTO genericMessagesDTO) {
        UsersDTO tempUserDto = new UsersDTO();
        tempUserDto.setCode(genericMessagesDTO.getUserCode());
        tempUserDto.setNameAr(genericMessagesDTO.getUserNameAr());
        tempUserDto.setNameEn(genericMessagesDTO.getUserNameEn());
        tempUserDto.setStatus(1);
        return usersService.save(tempUserDto);
    }
}
